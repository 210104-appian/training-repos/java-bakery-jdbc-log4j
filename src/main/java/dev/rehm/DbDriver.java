package dev.rehm;

import java.util.List;

import org.apache.log4j.Logger;

import dev.rehm.daos.BakeryItemDao;
import dev.rehm.daos.BakeryItemDaoImpl;
import dev.rehm.models.BakeryItem;

public class DbDriver {
	
	/*
	 * TRACE - lowest
	 * DEBUG
	 * INFO
	 * WARN
	 * ERROR
	 * FATAL - highest
	 */
	
	static Logger log = Logger.getRootLogger();
	
	public static void main(String[] args) {
//		log.info("Hello");
		BakeryItemDao biDao = new BakeryItemDaoImpl();
		
//		
		List<BakeryItem> items = biDao.getAllBakeryItems();
		for(BakeryItem item: items) {
			System.out.println(item);
		}
		
		
//		boolean success = biDao.changeItemPrice(5,2.50);
//		System.out.println(success);
		
//		List<String> types = biDao.getAllBakeryItemTypes();
//		System.out.println(types);
	}

}
