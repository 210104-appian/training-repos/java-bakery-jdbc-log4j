package dev.rehm.daos;

import java.util.List;

import dev.rehm.models.BakeryItem;

public interface BakeryItemDao {

	public List<BakeryItem> getAllBakeryItems();
	public List<String> getAllBakeryItemTypes(); // -> returns 'Muffin', 'Bread', 'Coffee' .... etc.
	public List<BakeryItem> getBakeryItemsByType(String type); //can use type to get all muffins e.g.
	public BakeryItem addNewBakeryItem(BakeryItem item);
	public boolean changeItemPrice(int itemId, double newPrice);
	public boolean removeItemById(int id);

}
