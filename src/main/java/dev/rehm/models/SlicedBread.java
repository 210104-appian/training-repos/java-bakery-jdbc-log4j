package dev.rehm.models;

public class SlicedBread extends Bread {
	
	private boolean isToasted;
	private String spread = "none";

	public SlicedBread() {
//		super("unknown");
		super();
	}
	
	public SlicedBread(boolean isToasted, String spread, String type) {
		super();
		this.type = type;
		this.isToasted = isToasted;
		this.spread = spread;
	}

	public boolean isToasted() {
		return isToasted;
	}

	public void setToasted(boolean isToasted) {
		this.isToasted = isToasted;
	}

	public String getSpread() {
		return spread;
	}

	public void setSpread(String spread) {
		this.spread = spread;
	}
	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (isToasted ? 1231 : 1237);
		result = prime * result + ((spread == null) ? 0 : spread.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		SlicedBread other = (SlicedBread) obj;
		if (isToasted != other.isToasted)
			return false;
		if (spread == null) {
			if (other.spread != null)
				return false;
		} else if (!spread.equals(other.spread))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SlicedBread [isToasted=" + isToasted + ", spread=" + spread + ", type=" + type + "]";
	}
	
	
	
}
