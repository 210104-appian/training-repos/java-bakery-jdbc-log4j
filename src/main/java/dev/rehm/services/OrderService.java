package dev.rehm.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import dev.rehm.daos.BakeryItemDao;
import dev.rehm.daos.BakeryItemDaoImpl;
import dev.rehm.models.BakeryItem;
import dev.rehm.models.Bread;
import dev.rehm.models.Coffee;
import dev.rehm.models.Muffin;
import dev.rehm.models.Order;
import dev.rehm.models.OrderStatus;
import dev.rehm.models.Size;

public class OrderService {
	
	private Scanner scanner = new Scanner(System.in);
	
	private BakeryItemDao biDao = new BakeryItemDaoImpl();
	
	public Order takeOrder() {
		Order order = new Order(OrderStatus.PENDING);
		String orderName = takeOrderName();
		order.setCustomerName(orderName);
		String orderPhone = takeOrderPhoneNumber();
		order.setPhone(orderPhone);
		
		ArrayList<BakeryItem> items = new ArrayList<>();
		String selection;
		do {
		BakeryItem item = takeOrderItem();
		items.add(item); 
		System.out.println("Would you like to order another item? (Y/N)");
		selection = scanner.next();
		} while (selection.toUpperCase().charAt(0)=='Y');
		order.setItems(items);
		return order;
	}

	
	private String takeOrderName() {
		System.out.println("Can we get a name for your order?");
		String name = scanner.next();
		return name;
	}
	
	private String takeOrderPhoneNumber() {
		System.out.println("Please enter phone number:");
		String phone = scanner.next();
		return phone;
	}

	
	private BakeryItem takeOrderItem() {
		System.out.println("Would you like to order:");
		List<String> itemTypes = biDao.getAllBakeryItemTypes();
		for(int i = 0; i<itemTypes.size(); i++) {
			System.out.println("["+(i+1)+"] "+itemTypes.get(i));
		}
		
		
		String selection = scanner.next();
		if(selection.matches("^\\d+$")) { // check to see if it matches a number
			int selectionInt = Integer.parseInt(selection);		
			if(selectionInt<=itemTypes.size() && selectionInt!=0) { // make sure it's within the bounds of our itemTypes
				
				String selectedType = itemTypes.get(selectionInt-1);
				List<BakeryItem> selectedTypeBakeryItems = biDao.getBakeryItemsByType(selectedType);
				System.out.println("Your options:");
				//successfully get a bakery item
				for(int i=0; i<selectedTypeBakeryItems.size(); i++) {
					System.out.println("["+(i+1)+"] " + selectedTypeBakeryItems.get(i).details());
				}
				
				String itemSelection = scanner.next();
				if(itemSelection.matches("^\\d+$")) { // check to see if it matches a number
					int itemSelectionInt = Integer.parseInt(itemSelection);		
					if(itemSelectionInt<=selectedTypeBakeryItems.size() && selectionInt!=0) {
						return selectedTypeBakeryItems.get(itemSelectionInt-1);
					}
				}
			}
		}
		
		System.out.println("Invalid input. Please try again.");
		return takeOrderItem();
	}

	
}
